const Router = require('express');
const router = new Router();
const noteController = require('../controllers/notes');

router.get('/', noteController.getNotes);
router.post('/', noteController.createNote);
router.get('/:id', noteController.getNote);
router.put('/:id', noteController.editNote);
router.patch('/:id', noteController.editNoteValue);
router.delete('/:id', noteController.deleteNote);

module.exports = router;
