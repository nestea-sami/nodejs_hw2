
const Router = require('express');
const router = new Router();
const authMiddleware = require('../middleware/authMiddleware');

const authRoutes = require('./authRoutes');
const userRoutes = require('./userRoutes');
const noteRoutes = require('./noteRoutes');

router.use('/auth', authRoutes);
router.use('/users', authMiddleware, userRoutes);
router.use('/notes', authMiddleware, noteRoutes);

module.exports = router;