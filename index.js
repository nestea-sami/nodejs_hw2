const dotenv = require('dotenv').config();
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');

const errorHandlingMiddleware = require('./middleware/errorHandlingMiddleware');
const appRoutes = require('./routes/indexRoutes');

const fs = require('fs');
const path = require("path");
const ApiError = require("./errors/allErrors");

const accessedLog = fs.createWriteStream(path.join(__dirname, 'requestLogs.log'), {flags: 'a'});

const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan('tiny', { stream: accessedLog }));

app.use('/api', appRoutes);

app.use(errorHandlingMiddleware);

const PORT = process.env.PORT || 8080;
const DB_USER = process.env.DB_USER;
const DB_PASSWORD = process.env.DB_PASSWORD;
const DB_NAME = process.env.DB_NAME;


async function start() {
    try {
        await mongoose.connect(
            `mongodb+srv://${DB_USER}:${DB_PASSWORD}@anastasiiasamiliukmongo.weebggm.mongodb.net/${DB_NAME}?retryWrites=true&w=majority`
        );
        app.listen(PORT, () => console.log(`Server started at port: ${PORT}`))
    } catch (err) {
        console.log(err);
    }
}

start();

