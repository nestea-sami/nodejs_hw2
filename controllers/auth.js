const User = require('../models/UserModel');
const ApiError = require('../errors/allErrors');
const bcrypt = require('bcryptjs');
const generateJWT = require('../helper/generateJWT');

class AuthController {
    async register ( req, res, next ) {
        try {
            const { username, password } = req.body;

            if( !username || !password ) {
                return next(ApiError.badRequest('Username or password is not provided'))
            }

            const isUser = await User.findOne({ username});

            if( isUser ) {
                return next(ApiError.badRequest('Username is already taken'));
            }

            const hashedPassword = await bcrypt.hash(password, 10);
            const newUser = new User({
                username,
                password: hashedPassword
            });

            await newUser.save();

            const token = generateJWT(username, newUser.id);

            res.status(200).json({message: "Success", token});

        } catch (err) {
            return next(ApiError.internalError('Server error'));
        }
    }

    async login ( req, res, next ) {
        try {
            const { username, password } = req.body;

            if( !username || !password ) {
                return next(ApiError.badRequest('Username or password is not provided'))
            }

            const user = await User.findOne({ username });

            if( !user ) {
                return next(ApiError.badRequest('Username or password is incorrect'));
            }

            const isPasswordCorrect = await bcrypt.compare(password, user.password);

            if( !isPasswordCorrect ) {
                return next(ApiError.badRequest('Username or password is incorrect'));
            }

            const token = generateJWT(user.username, user.id);

            res.status(200).json({ message: "Success", jwt_token: token });

        } catch(err) {
            return next(ApiError.internalError('Server error'));
        }
    }
}

module.exports = new AuthController();