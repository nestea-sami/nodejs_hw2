const Note = require('../models/NoteModel');
const ApiError = require('../errors/allErrors');

class NoteController {
    async getNotes(req, res, next) {
        try {
            const {offset = 0, limit = 0} = req.query;

            const notes = await Note.find({userId: req.user.id}).skip(offset).limit(limit);

            res.status(200).json({offset, limit, count: notes.length, notes});

        } catch (err) {
            return next(ApiError.internalError(err.message));
        }
    }

    async createNote(req, res, next) {
        try {
            const {text} = req.body;

            if (!text) {
                return next(ApiError.badRequest('Text for note is not provided'));
            }

            const newNote = new Note({
                text,
                userId: req.user.id
            });

            await newNote.save();

            res.status(200).json({
                id: newNote.id,
                text: newNote.text,
                completed: newNote.completed,
                createdAt: newNote.createdAt,
                message: "Success"
            });
        } catch (err) {
            return next(ApiError.internalError('Server error'));
        }
    }

    async getNote(req, res, next) {
        try {
            const _id = req.params.id;

            if (!_id) {
                return next(ApiError.badRequest('id is not provided'));
            }

            const note = await Note.findOne({_id});

            if (!note) {
                return next(ApiError.badRequest('There is no note with such id'));
            }

            if (note.userId !== req.user.id) {
                return next(ApiError.forbidden('Forbidden'));
            }

            res.status(200).json({note});
        } catch (err) {
            return next(ApiError.internalError('Server error'));
        }
    }

    async editNote(req, res, next) {
        try {
            const _id = req.params.id;
            const {text} = req.body;

            if (!_id) {
                return next(ApiError.badRequest('id is not provided'));
            }

            const note = await Note.findOne({_id});

            if (!note) {
                return next(ApiError.badRequest('There is no note with such id'));
            }

            if (note.userId !== req.user.id) {
                return next(ApiError.forbidden('Forbidden'));
            }


            const newNote = await Note.findByIdAndUpdate(_id, {text});

            res.status(200).json({
                text: newNote.text,
                completed: newNote.completed,
                createdAt: newNote.createdAt,
                updatedAt: newNote.updatedAt,
                message: "Success"
            });

        } catch (err) {
            return next(ApiError.internalError(err.message))
        }
    }

    async editNoteValue(req, res, next) {
        try {
            const _id = req.params.id;

            if (!_id) {
                return next(ApiError.badRequest('id is not provided'));
            }

            const note = await Note.findOne({_id});

            if (!note) {
                return next(ApiError.badRequest('There is no note with such id'));
            }

            if (note.userId !== req.user.id) {
                return next(ApiError.forbidden('Forbidden'));
            }

            await Note.findByIdAndUpdate(_id, {completed: !note.completed})

            res.status(200).json({message: "Success"});
        } catch (err) {
            return next(ApiError.internalError('Server error'));
        }
    }

    async deleteNote(req, res, next) {
        try {
            const _id = req.params.id;

            if (!_id) {
                return next(ApiError.badRequest('id is not provided'));
            }

            const note = await Note.findOne({_id});

            if (!note) {
                return next(ApiError.badRequest('There is no note with such id'));
            }

            if (note.userId !== req.user.id) {
                return next(ApiError.forbidden('Forbidden'));
            }

            await Note.findByIdAndDelete(_id);

            res.status(200).json({message: "Success"});

        } catch (err) {
            return next(ApiError.internalError('Server error'));
        }
    }
}

module.exports = new NoteController();